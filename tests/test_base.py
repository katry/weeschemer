# -*- coding: utf-8 -*-
from os import path
from logging import DEBUG, INFO, WARN, ERROR, FATAL
from weeschemer import Rule, Model


class LoggerModel(Model):
	name = Rule(str, min_length=3)
	screen = Rule(bool, optional=True, default=True)
	logdir = Rule(str, optional=True, call=lambda x: path.isdir(x))
	level = Rule(
		int,
		optional=True,
		call=lambda x: x in [DEBUG, INFO, WARN, ERROR, FATAL]
	)
	formatter = Rule(str, optional=True)


class ExchangeModel(Model):
	name = Rule(str, min_length=3)
	symbols = Rule(list, optional=True)
	intervals = Rule(list, optional=True)


class ConfigModel(Model):
	workdir = Rule(str, call=lambda x: path.isdir(x))
	exchanges = Rule(list, model=ExchangeModel)
	loggers = Rule(list, optional=True, model=LoggerModel, default=[])


basic_data = {
	"workdir": "/tmp",
	"exchanges": [
		{
			"name": "binance",
			"symbols": ["BTCUSDT", "ETHUSDT", "ETHBTC"],
			"intervals": ["1m", "3m", "5m", "15m", "1h", "2h"]
		},
		{
			"name": "coinbase",
			"symbols": ["BTCUSDT", "ETHUSDT", "ETHBTC"],
			"intervals": ["1h", "2h", "4h", "6h", "12h", "1d"]
		}
	]
}

invalid_data = {
	"workdir": "/tmp"
}


def test_config_model_valid():
	try:
		config = ConfigModel(basic_data)
	except (ValueError, KeyError):
		assert False

	assert config.workdir == "/tmp"
	assert config.exchanges[0].name == "binance"
	assert config.exchanges[1].name == "coinbase"
	assert config.loggers == []


def test_config_model_failed():
	try:
		ConfigModel(invalid_data)
	except (ValueError, KeyError):
		assert True
		return

	assert False


