# weeschemer - Work in progress

Simple Python schema validation library written in Rust.

## Instalation

TBD

## Structure

### Model

Model model class specifies complete validation of the data structure, instancing of class creates validated data model.

<b>Initialization parameters:</b>
- `values` - dict of values to validate (optional)


<b>Methods:</b>
- `check(values: dict)` - method validates dict given as a parameter

### Rule

Rule class is intended to specify validation exact field of structure and used in model class as class propery assigned do name matching structure key.

<b>Initialization parameters:</b>
- `type` - positional parameter used to determine type of validated field
- `min` - minimal value - used for numeric types (optional)
- `max` - maximal value - used for numeric types (optional)
- `length` - exact length - used for strings, lists and tuples (optional)
- `min_length` - min length - used for strings, lists and tuples (optional)
- `max_length` - max length - used for strings, lists and tuples (optional)
- `regex` - regex string - used for strings (optional)
- `call` - validation function - takes as argument validating value and retuns bool (optional)
- `model` - TBD

## Example

```py
from os import path
from weeschemer import Rule, Model


# validator specification
class LoggerModel(Model):
	name = Rule(str, min_length=3)
	screen = Rule(bool, optional=True, default=True)
	logdir = Rule(str, optional=True, call=lambda x: path.isdir(x))
	level = Rule(
		int,
		optional=True,
		call=lambda x: x in [DEBUG, INFO, WARN, ERROR, FATAL]
	)
	formatter = Rule(str, optional=True)


# validator specification
class ExchangeModel(Model):
	name = Rule(str, min_length=3)
	symbols = Rule(list, optional=True)
	intervals = Rule(list, optional=True)


# more complex validator specification using other validators
class ConfigModel(Model):
	workdir = Rule(str, call=lambda x: path.isdir(x))
	exchanges = Rule(list, model=ExchangeModel)
	loggers = Rule(list, optional=True, model=LoggerModel)


# input data
data = {
	"workdir": "/tmp",
	"exchanges": [
		{
			"name": "binance",
			"symbols": ["BTCUSDT", "ETHUSDT", "ETHBTC"],
			"intervals": ["1m", "3m", "5m", "15m", "1h", "2h"]
		},
		{
			"name": "coinbase",
			"symbols": ["BTCUSDT", "ETHUSDT", "ETHBTC"],
			"intervals": ["1h", "2h", "4h", "6h", "12h", "1d"]
		}
	]
}


# check config data validity
try:
	config = ConfigModel(data)
except ValueError:
	print("validation failed")

# config object contains valid data
assert config.workdir == "/tmp"
assert config.exchanges[0].name == "binance"
assert config.exchanges[1].name == "coinbase"
assert config.loggers == []
```

## License

Created by Patrik Katreňák and released under General Public License v3.0.
