# -*- coding: utf-8 -*-
# import weeschemer
from .weeschemer import *


class Model(weeschemer.__Model):
	def __getattribute__(self, attr):
		if attr in ["values", "rules", "check", "__dict__"]:
			return super().__getattribute__(attr)
		elif attr in self.values:
			return self.values[attr]
		else:
			raise KeyError("Attribute not found {}".format(attr))


__all__ = [Model, weeschemer.Rule]
