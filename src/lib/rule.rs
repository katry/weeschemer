
use regex::Regex;
use pyo3::prelude::*;
use pyo3::types::{PyList, PyTuple, PyType};
use pyo3::exceptions::PyValueError;


#[pyclass(get_all)]
#[derive(FromPyObject)]
pub struct Rule {
	py_type: Py<PyType>,
	optional: bool,
	min: Option<i64>,
	max: Option<i64>,
	length: usize,
	min_length: usize,
	max_length: usize,
	regex: Option<String>,
	call: Option<Py<PyAny>>,
	model: Option<Py<PyType>>,
	pub default: Option<Py<PyAny>>,
	value: Option<Py<PyAny>>,
}
unsafe impl Send for Rule {}

#[pymethods]
impl Rule {
	#[new]
	#[pyo3(signature = (py_type, optional=false, min=None, max=None, length=0, min_length=0, max_length=0, regex=None, default=None, call=None, model=None))]
	fn new(py_type: Py<PyType>, optional: bool, min: Option<i64>, max: Option<i64>, length: usize, min_length: usize, max_length: usize, regex: Option<&str>,default: Option<Py<PyAny>>, call: Option<Py<PyAny>>, model: Option<Py<PyType>>) -> PyResult<Self> {
		let mut re: Option<String> = None;
		if regex.is_some() {
			re = Some(regex.unwrap().to_string());
		}
		if call.is_some() {
			let not_callable = Python::with_gil(|py| {
				!call.as_ref().unwrap().bind(py).is_callable()
			});
			if not_callable {
				return Err(PyValueError::new_err("Validation function is not callable"));
			}
		}

		Ok(Self{
			py_type: py_type,
			optional: optional,
			min: min,
			max: max,
			length: length,
			min_length: min_length,
			max_length: max_length,
			regex: re,
			default: default.clone(),
			call: call,
			model: model,
			value: default.clone(),
		})
	}

	pub fn check(&mut self, value: Py<PyAny>) -> bool {
		let result = Python::with_gil(|py| {
			let mut val = value.bind(py);
			let py_type = self.py_type.bind(py).name().unwrap();
			if val.is_none() && self.default.is_some() {
				val = self.default.as_ref().unwrap().bind(py);
			}
			let binding = val.get_type();
			let val_type = binding.name().unwrap();
			if val_type != py_type {
				return false;
			}
			if val_type == "int" || val_type == "float" {
				let intval = val.extract().unwrap();
				if self.min.is_some() && self.min.unwrap() > intval {
					return false;
				}
				if self.max.is_some() && self.max.unwrap() < intval {
					return false;
				}
			}
			else if val_type == "str" {
				let strval: &str = val.extract().unwrap();
				if self.length > 0 && strval.len() != self.length {
					return false;
				}
				if self.min_length > 0 && strval.len() < self.min_length {
					return false;
				}
				if self.max_length > 0 && strval.len() > self.max_length {
					return false;
				}
				if val_type == "str" && self.regex.is_some() {
					let regex = Regex::new(&self.regex.as_ref().unwrap()[..]).unwrap();
					if !regex.is_match(val.extract().unwrap()) {
						return false;
					}
				}
			}
			if self.call.is_some() {
				let args = (val,);
				let res = self.call.as_ref().unwrap().bind(py).call1(args).unwrap();
				if !res.into_any().is_truthy().unwrap() {
					return false;
				}
			}
			if self.model.is_some() {
				if val_type == "dict" {
					let model = self.model.as_ref().unwrap().bind(py).call0().unwrap();
					let result = model.call_method("check", (val.clone().unbind(),), None);
					if result.is_err() {
						return false;
					}
					self.value = Some(model.unbind().clone());
				}
				else if val_type == "list" {
					let list = val.downcast::<PyList>().unwrap();
					let values = PyList::empty_bound(py);
					for iter in list.iter() {
						let model = self.model.as_ref().unwrap().bind(py).call0().unwrap();
						let result = model.call_method("check", (iter,), None);
						if result.is_err() {
							return false;
						}
						let _ = values.append(model.clone());
					}
					self.value = Some(<pyo3::Bound<'_, pyo3::PyAny> as Clone>::clone(&values.downcast::<PyAny>().unwrap()).unbind());
				}
				else if val_type == "tuple" {
					let tuple = val.downcast::<PyTuple>().unwrap();
					let values = PyList::empty_bound(py);
					for iter in tuple.iter() {
						let model = self.model.as_ref().unwrap().bind(py).call0().unwrap();
						let result = model.call_method("check", (iter,), None);
						if result.is_err() {
							return false;
						}
						let _ = values.append(model.clone());
					}
					self.value = Some(<pyo3::Bound<'_, pyo3::PyAny> as Clone>::clone(&values.downcast::<PyAny>().unwrap()).unbind());
				}
			}
			else {
				self.value = Some(val.clone().unbind());
			}
			return true;
		});
		return result;
	}

	pub fn is_optional(&self) -> bool {
		return self.optional;
	}

	pub fn _get_val(&self) -> Option<Py<PyAny>> {
		return self.value.clone();
	}
}
