use pyo3::types::{PyType, PyDict};
use pyo3::exceptions::{PyKeyError, PyValueError};
use std::collections::HashMap;
use crate::lib::rule::Rule;
use pyo3::prelude::*;


#[pyclass(get_all, dict, subclass)]
#[derive(FromPyObject, Debug)]
pub struct __Model {
	rules: HashMap<String, Py<Rule>>,
	values: HashMap<String, Option<Py<PyAny>>>,
}
unsafe impl Send for __Model {}


#[pymethods]
impl __Model {
	#[new]
	#[classmethod]
	#[pyo3(signature = (values=None))]
	fn new(cls: &PyType, values: Option<Py<PyDict>>) -> PyResult<Self> {
		let mut rule_map: HashMap<String, Py<Rule>> = HashMap::new();
		for iter in cls.getattr("__dict__").unwrap().iter().unwrap() {
			if iter.is_ok() {
				let key: String = iter.unwrap().extract().unwrap();
				let item = cls.getattr(&key[..]).unwrap();
				let mn = item.get_type().name().unwrap();
				if mn != "builtins.Rule" {
					continue;
				}
				let rule = item.extract().unwrap();
				rule_map.insert(key, rule);
			}
		}

		let mut instance = Self{
			rules: rule_map,
			values: HashMap::new(),
		};
		if values.is_some() {
			let result = instance.check(values.unwrap());
			if result.is_err() {
				return Err(result.err().unwrap());
			}
		}
		Ok(instance)
	}

	pub fn check(&mut self, values: Py<PyDict>) -> PyResult<()> {
		let result = Python::with_gil(|py| {
			self.values = values.bind(py).extract().unwrap();
			for key in self.rules.keys() {
				let rule = self.rules[key].bind(py);
				if !self.values.contains_key(key) {
					if !rule.call_method("is_optional", (), None).unwrap().extract::<bool>().unwrap() {
						return Err(PyKeyError::new_err("Missing key `".to_owned() + &key[..] + "`"));

					}
				}
				else if !rule.call_method("check", (self.values[key].clone(),), None).unwrap().extract::<bool>().unwrap() {
						return Err(PyValueError::new_err("Invalid value for key `".to_owned() + &key[..] + "`"));
				}
				let val = Some(rule.call_method("_get_val", (), None).unwrap().unbind());
				self.values.insert(key.to_string(), val);
			}
			Ok(())
		});
		return result;
	}
}
