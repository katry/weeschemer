use crate::lib::rule::Rule;
use crate::lib::model::__Model;
use pyo3::prelude::*;

mod lib {
	pub mod rule;
	pub mod model;
}


#[pymodule]
fn weeschemer(_py: Python<'_>, m: &Bound<'_, PyModule>) -> PyResult<()> {
	m.add_class::<Rule>()?;
	m.add_class::<__Model>()?;
	Ok(())
}
